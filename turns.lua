-- This file is part of Gladstone, a TBT framework built atop the LÖVE framework
-- Copyright (C) 2017  Aidan Gauland
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/>.

local exports = {}

local current_folder = (...):gsub('%.[^%.]+$', '')
local board_util = require(current_folder .. ".board")
local ai = require(current_folder .. ".ai")

function exports.nextTurn(board)
  -- Trigger the onTurn event for each AI unit.
  for _, unit in pairs(board.units) do
    if board_util.isAIUnit(unit) then
      ai.onTurn(unit.ai)
    end
  end

  -- Reset the moved flag for every player unit
  for _, unit in pairs(board.units) do
    if board_util.isUserUnit(unit) then
      unit.moved = false
    end
  end
end

return exports
