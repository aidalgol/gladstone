=========
Gladstone
=========
A TBT framework built atop LÖVE
-------------------------------

Gladstone is a framework for building turn-based tactics games in the LÖVE_ framework.  (It doesn't do much yet.)  Look at the `demo game`_ to get an idea of what it can do.

.. _LÖVE: https://love2d.org/
.. _demo game: https://gitlab.com/aidalgol/gladstone-demo
