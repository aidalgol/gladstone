-- This file is part of Gladstone, a TBT framework built atop the LÖVE framework
-- Copyright (C) 2017  Aidan Gauland
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/>.

local lume = require "lume"

local current_folder = (...):gsub('%.[^%.]+$', '')
local board_util = require(current_folder .. ".board")

local HEADINGS = {-90, 0, 90}

local function onSpottedUnits(watcher, _)
  print("AI unit at (" .. watcher.coords.x .. ", " .. watcher.coords.y .. ") spotted someone.")
end

local exports = {}

-- Alternate between looking 90° to the left and right of the unit's initial
-- heading (pausing at 0° in between each alternation).
function exports.makeWatcher(coords, board)
  local unit = board.units[coords]
  local result = {
    coords = coords,
    board = board,
    initialHeading = unit.heading,
    headingIndex = lume.pingpongseq(3)
  }
  -- Register this object with the given unit.
  unit.ai = result
  return result
end

-- It is this unit's turn
function exports.onTurn(watcher)
  -- Turn
  local i = watcher.headingIndex()
  watcher.board.units[watcher.coords].heading = watcher.initialHeading + HEADINGS[i]

  -- Trigger event when we have spotted a unit belonging to another unit.
  local spottedUnits = lume.filter(board_util.unitsVisibleFrom(watcher.board, watcher.coords.x, watcher.coords.y),
                                   function(unit)
                                     return board_util.isUserUnit(watcher.board.units[unit])
                                   end)
  if table.maxn(spottedUnits) > 0 then
    onSpottedUnits(watcher, spottedUnits)
  end
end

return exports
