-- This file is part of Gladstone, a TBT framework built atop the LÖVE framework
-- Copyright (C) 2017  Aidan Gauland
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/>.

local bresenham = require "bresenham"
local lume = require "lume"
local pt_table = require "point-table"

local SIGHT_DISTANCE = 7
local SIGHT_ANGLE = 90 -- degrees
local ACTION_POINTS = 3

local exports = {debug_callbacks = {}}

-- Takes a unit, and relative polar coordinates to a point, and returns whether
-- the point is within the unit's field of vision.
local function isPointWithinCOV(unit, distance, angle)
  -- TODO: Use the unit's field-of-vision attribute.
  return
    distance <= SIGHT_DISTANCE and
    lume.betweenangles(math.deg(angle), unit.heading - (SIGHT_ANGLE / 2), unit.heading + (SIGHT_ANGLE / 2))
end

-- COV = Cone Of Vision
function exports.isPointWithinCOV(board, x1, y1, x2, y2)
  local r = lume.distance(x1, y1, x2, y2)
  local phi = lume.angle(x1, y1, x2, y2)
  return isPointWithinCOV(board.units[{x1, y1}], r, phi)
end

function exports.makeBoard(width, height)
  assert(width > 0)
  assert(height > 0)

  -- See the header comment in point_table.lua for an explanation of the data structure.
  local board = {
    width = width,
    height = height,
    units = pt_table.makePointTable(),
    barriers = pt_table.makePointTable(),
  }

  return board
end

-- Returns whether (x2, y2) is visible from (x1, y1).
function exports.lineOfSight(board, x1, y1, x2, y2)
  local function noBarrierAt(x, y)
    return board.barriers[{x,y}] == nil
  end

  if DEBUG then
    local points, result = bresenham.line(x1, y1, x2, y2, noBarrierAt)
    exports.debug_callbacks.lineOfSight(points)
    return result
  else
    return bresenham.los(x1, y1, x2, y2, noBarrierAt)
  end
end

function exports.isPointOnBoard(board, x, y)
  return 0 <= x and x < board.width and
         0 <= y and y < board.height
end

function exports.moveUnit(board, x_unit, y_unit, x_dest, y_dest)
  if board.units[{x_dest, y_dest}] ~= nil then
    error("Destination not empty")
  end
  board.units[{x_dest, y_dest}] = board.units[{x_unit, y_unit}]
  board.units[{x_unit, y_unit}] = nil
end

-- Return an array of points (not the units themselves) visible to the unit at (x, y).
function exports.unitsVisibleFrom(board, x, y)
  if board.units[{x,y}] == nil then
    error(lume.format("No unit at ({1}, {2})", {x, y}))
  end
  local visibleUnits = {}
  for pointString,_ in pairs(board.units) do
    local point = pt_table.keyToTable(pointString)
    -- Skip the unit we're running on (i.e. the unit at (x,y)).
    if not (point.x == x and point.y == y) then
      -- Get the relative polar coordinates from (x,y) to this unit.
      if isPointWithinCOV(board.units[{x,y}],
                           lume.distance(x, y, point.x, point.y),
                           lume.angle(x, y, point.x, point.y))
      then
        if exports.lineOfSight(board, x, y, point.x, point.y) then
          table.insert(visibleUnits, point)
        end
      end
    end
  end
  return visibleUnits
end

function exports.validUnitMoves(board, x_start, y_start)
  local function pointToString(x, y)
    return tostring(x) .. "," .. tostring(y)
  end

  -- AP = Action Points
  local function traverse(ap, x, y)
    local result = {}
    if not (x == x_start and y == y_start) and
       exports.isPointOnBoard(board, x, y) and
       board.barriers[{x,y}] == nil
    then
      result = {pointToString(x, y)}
    end
    if ap < 1 then
      return result
    else
      return lume.concat(
        result,
        traverse(ap - 1, x,     y + 1),
        traverse(ap - 1, x,     y - 1),
        traverse(ap - 1, x + 1, y    ),
        traverse(ap - 1, x - 1, y    ))
    end
  end

  return lume.map(lume.set(traverse(ACTION_POINTS, x_start, y_start)),
             function(x) return lume.map(lume.split(x, ","), tonumber) end)
end

function exports.isUserUnit(unit)
  -- Exactly one of the "ai" or "moved" fields will be set, otherwise there is a
  -- serious bug somewhere.
  assert((unit.ai == nil or unit.moved == nil)
      and not (unit.ai == nil and unit.moved == nil))

  -- Only AI units have the "ai" field.
  return unit.ai == nil
end

function exports.isAIUnit(unit)
  return not exports.isUserUnit(unit)
end

return exports
