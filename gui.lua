-- This file is part of Gladstone, a TBT framework built atop the LÖVE framework
-- Copyright (C) 2017  Aidan Gauland
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/>.

local lume = require "lume"
local suit = require "suit"
local pt_table = require "point-table"

local current_folder = (...):gsub('%.[^%.]+$', '')
local board_util = require(current_folder .. ".board")
local turns = require(current_folder .. ".turns")

--- GUI constants ---
local GRID_TOP_LEFT_OFFSET =  8
local GRID_LINE_WIDTH      =  2

local LABEL_COLOR = {normal = {fg = {0.0, 0.0, 0.0}}}

local WIDGETS_TOP_LEFT_MARGIN =   5
local WIDGETS_PADDING_X       =  10
local WIDGETS_PADDING_Y       =   5

local BUTTON_WIDTH            = 300
local BUTTON_HEIGHT           =  30

local COLORS = {
  BARRIERS          = {0.48, 0.24, 0.07},
  REACHABLE_SQUARES = {0.07, 0.93, 0.03, 0.39},
  VISIBLE_SQUARES   = {0.40, 1.00, 0.99, 0.20}
}

local exports = {board = {}}

local function moveUnit(state, board, clickedGridSquare)
  if state.board.selected ~= nil and
    board.units[state.board.selected] ~= nil and
    clickedGridSquare.x ~= state.board.selected.x and
    clickedGridSquare.y ~= state.board.selected.y and -- Dest square not selected square
    board.barriers[clickedGridSquare] == nil -- No barrier at dest square
  then
    if not board_util.isUserUnit(board.units[state.board.selected]) then
      state.message = "That unit does not belong to the player."
    elseif board.units[clickedGridSquare] ~= nil then
      state.message = "The destination square is occupied."
    elseif lume.all(board_util.validUnitMoves(board, state.board.selected.x, state.board.selected.y),
                    function(p)
                      return not (p[1] == clickedGridSquare.x and p[2] == clickedGridSquare.y)
                    end)
    then
      state.message = "The destination square is out of range."
    elseif board.units[state.board.selected].moved then
      state.message = "That unit has already moved"
    else
      local unit = board.units[state.board.selected]
      board_util.moveUnit(board,
                          state.board.selected.x, state.board.selected.y,
                          clickedGridSquare.x, clickedGridSquare.y)
      unit.moved = true
      state.board.selected = nil
      -- Clear the old message
      state.message = nil
    end
  end
end

local function boardCoordAtPixelCoord(coord, state)
  return math.floor((coord - GRID_TOP_LEFT_OFFSET) / state.board.gridSquareSize)
end

local function getUnitColor(unit)
  if board_util.isUserUnit(unit) then
    return 0.96, 0.15, 0.15
  else
    return 0.20, 0.50, 1.00
  end
end

local function clearMarks(state)
  for k,_ in pairs(state.board.markedPoints) do
    state.board.markedPoints[k] = {}
  end
end

local function drawBoard(board, state)
  local gridSquareInsideSize = state.board.gridSquareSize - 2

  local function fillSquare(x, y)
    love.graphics.rectangle("fill",
                            (x * state.board.gridSquareSize) + 1, (y * state.board.gridSquareSize) + 1,
                            gridSquareInsideSize, gridSquareInsideSize)
  end

  love.graphics.setColor(0.0, 0.0, 0.0) -- Black
  love.graphics.rectangle("line",
                          0, 0,
                          board.width * state.board.gridSquareSize, board.height * state.board.gridSquareSize)
  -- Draw vertical lines
  for i = 1, board.width - 1 do
    local x = i * state.board.gridSquareSize
    love.graphics.line(x, 0,
                       x, board.height * state.board.gridSquareSize)
  end
  -- Draw horizontal lines
  for i = 1, board.height - 1 do
    local y = i * state.board.gridSquareSize
    love.graphics.line(0, y,
                       board.width * state.board.gridSquareSize, y)
  end
  -- Draw board items
  for i = 0, board.width - 1 do
    for j = 0, board.height - 1 do
      -- Draw units
      if board.units[{i,j}] ~= nil then
        local radius = (state.board.gridSquareSize - state.board.gridSquarePadding) / 2
        local function calculateOffset(coord)
          return (state.board.gridSquareSize * coord) + radius + (state.board.gridSquarePadding / 2)
        end
        local x = calculateOffset(i)
        local y = calculateOffset(j)
        love.graphics.setColor(getUnitColor(board.units[{i,j}]))
        love.graphics.circle("fill", x, y, radius, 20)
        -- Draw a point at the edge of the circle along the heading vector.
        love.graphics.push()
        local ARROW_LENGTH = state.board.gridSquarePadding / 2
        local ARROW_WIDTH = 10
        love.graphics.setColor(0.0, 0.0, 0.0) -- Black
        love.graphics.translate(x, y)
        love.graphics.rotate(math.rad(board.units[{i,j}].heading))
        love.graphics.translate(radius, 0)
        love.graphics.polygon("fill",
                              0, -ARROW_WIDTH / 2,
                              ARROW_LENGTH, 0,
                              0, ARROW_WIDTH / 2)
        love.graphics.pop()
      end

      -- Draw barriers
      if board.barriers[{i,j}] ~= nil then
        love.graphics.setColor(COLORS.BARRIERS)
        fillSquare(i, j)
      end

      -- Highlight squares visible by any AI unit.
      for point_string,unit in pairs(board.units) do
        if board_util.isAIUnit(unit) then
          local point = pt_table.keyToTable(point_string)
          if board_util.isPointWithinCOV(board, point.x, point.y, i, j) and
             board_util.lineOfSight(board, point.x, point.y, i, j) then
            love.graphics.setColor(COLORS.VISIBLE_SQUARES)
            fillSquare(i, j)
            -- We don't need to check any other AI units because we've already
            -- determined that this square is to be highlighted.
            break
          end
        end
      end
    end
  end

  -- Draw coloured border just inside selected square (inset by one pixel).
  if state.board.selected ~= nil then
    local top  = state.board.selected.x * state.board.gridSquareSize
    local left = state.board.selected.y * state.board.gridSquareSize
    love.graphics.setColor(0.0, 1.0, 0.0)
    love.graphics.rectangle("line", top + 1, left + 1,
                            gridSquareInsideSize, gridSquareInsideSize)
    -- And highlight the squares reachable by the selected unit (if any), but
    -- only if it is a player unit.
    local unit = board.units[state.board.selected]
    if unit ~= nil and board_util.isUserUnit(unit) then
      for _,v in ipairs(board_util.validUnitMoves(board, state.board.selected.x, state.board.selected.y)) do
        love.graphics.setColor(COLORS.REACHABLE_SQUARES)
        fillSquare(v[1], v[2])
      end
    end
  end
end

function exports.updateSUIT(state, board)
  local BOARD_BOTTOM = (board.height * state.board.gridSquareSize) + (GRID_LINE_WIDTH * board.height / 2)
  suit.layout:reset(GRID_TOP_LEFT_OFFSET + GRID_LINE_WIDTH + WIDGETS_TOP_LEFT_MARGIN,
                    BOARD_BOTTOM + WIDGETS_TOP_LEFT_MARGIN,
                    WIDGETS_PADDING_X, WIDGETS_PADDING_Y)

  if suit.Button("Next Turn", suit.layout:row(BUTTON_WIDTH, BUTTON_HEIGHT)).hit then
    clearMarks(state)
    turns.nextTurn(board)
  end

  if state.pointUnderMouse then
    suit.layout:push(GRID_TOP_LEFT_OFFSET + WIDGETS_TOP_LEFT_MARGIN,
                     BOARD_BOTTOM + WIDGETS_TOP_LEFT_MARGIN + BUTTON_HEIGHT + WIDGETS_PADDING_Y * 2,
                     WIDGETS_PADDING_X, WIDGETS_PADDING_Y)
    suit.Label("(" .. state.pointUnderMouse.x .. ", " .. state.pointUnderMouse.y .. ")",
               {align = "left",
                color = LABEL_COLOR},
               suit.layout:col(200, 30))
    suit.layout:pop()
  end

  if state.message then
    suit.Label(state.message, {align = "left", color = LABEL_COLOR}, suit.layout:col(600, 30))
  end
end

function exports.draw(state, board)
  -- Draw board
  love.graphics.push()
  love.graphics.translate(GRID_TOP_LEFT_OFFSET, GRID_TOP_LEFT_OFFSET)
  love.graphics.setLineWidth(GRID_LINE_WIDTH)
  drawBoard(board, state)
  love.graphics.setColor(0.0, 1.0, 0.0)
  love.graphics.pop()

  suit.draw()
end

function exports.onMousePress(state, board, x, y, button)
  local clickedGridSquare = {x = boardCoordAtPixelCoord(x, state), y = boardCoordAtPixelCoord(y, state)}

  -- Ignore clicks outside the board dimensions
  if board_util.isPointOnBoard(board, clickedGridSquare.x, clickedGridSquare.y)
  then
    if button == 1 then
      -- On left click, mark the clicked grid square as selected.
      state.board.selected = clickedGridSquare
    elseif button == 2 then
      -- On right click, move the unit in the selected square (if any) to the
      -- clicked square, and clear the selection.
      moveUnit(state, board, clickedGridSquare)
    end
  end
end

function exports.onMouseMove(state, board, x, y, _, _)
  local newPointUnderMouse = {x = boardCoordAtPixelCoord(x, state), y = boardCoordAtPixelCoord(y, state)}

  if board_util.isPointOnBoard(board, newPointUnderMouse.x, newPointUnderMouse.y) then
    state.pointUnderMouse = newPointUnderMouse
  else
    state.pointUnderMouse = nil
  end
end

return exports
